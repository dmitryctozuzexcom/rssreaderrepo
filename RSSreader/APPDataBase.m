//
//  APPDataBase.m
//  RSSreader
//
//  Created by Mac on 07.02.16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "APPDataBase.h"

@implementation APPDataBase

+(id)sharedDataBase
{
    static APPDataBase *sharedMyDataBase = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyDataBase = [[self alloc] init];
    });
    return sharedMyDataBase;
}

-(void)setUpDataBase{
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    documentPaths = [paths objectAtIndex:0];
    plistPath = [documentPaths stringByAppendingPathComponent:@"dataTable.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"dataTable" ofType:@"plist"];
    }
    dataTable = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
}

-(NSString*)loadDataWithKey:(NSString *)key{
    return [dataTable objectForKey:key];
}

-(void)saveData:(NSString *)data WithKey:(NSString *)key{
    [dataTable setValue:data forKey:key];
    [dataTable writeToFile:plistPath atomically:YES];
}

-(void)saveArrayData:(NSArray *)data WithKey:(NSString *)key{
    [dataTable setObject:data forKey:key];
    [dataTable writeToFile:plistPath atomically:YES];
}

-(NSDictionary*)dataTable{
    return dataTable;
}

@end
