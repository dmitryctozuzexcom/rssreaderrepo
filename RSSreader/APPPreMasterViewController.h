//
//  APPPreMasterViewController.h
//  RSSreader
//
//  Created by Mac on 02.02.16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APPPreMasterViewController : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *sourceTableView;

@property (strong, nonatomic)
NSMutableArray *cellSelected;

@property (weak, nonatomic) IBOutlet UIButton *sendButton;



@end
