//
//  APPDataBase.h
//  RSSreader
//
//  Created by Mac on 07.02.16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APPDataBase : NSObject
{
    //DB
    NSArray *paths;
    NSString *documentPaths;
    NSString *plistPath;
    NSMutableDictionary *dataTable;
}

+(id)sharedDataBase;

-(void)setUpDataBase;

-(NSString*)loadDataWithKey:(NSString *)key;

-(void)saveData:(NSString*)data WithKey:(NSString *)key;

-(void)saveArrayData:(NSArray*)data WithKey:(NSString *)key;

-(NSDictionary*)dataTable;

@end
