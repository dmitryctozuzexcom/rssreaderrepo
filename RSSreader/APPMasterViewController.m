//
//  APPMasterViewController.m
//  RSSreader
//
//  Created by Rafael Garcia Leiva on 08/04/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "APPMasterViewController.h"
#import "APPDetailViewController.h"
#import "Reachability.h"
#import "APPDataBase.h"

#define TICK NSDate *startTime = [NSDate date]
#define TOCK NSLog(@"Function: %@, Time: %f", NSStringFromSelector(_cmd), -[startTime timeIntervalSinceNow])

@interface APPMasterViewController () {
    NSXMLParser *parser;
    NSMutableArray *feeds;
    NSMutableDictionary *item;
    NSMutableString *title;
    NSMutableString *link;
    NSString *element;
    NSMutableString *descr;
    NSMutableString *pubDate;
    NSArray *recievedArray;
    
    Reachability *internetReachableFoo;
    BOOL isOnline;
}
@property (nonatomic, strong) NSMutableArray *searchResult;
@end

@implementation APPMasterViewController

@synthesize recievedURL;


- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    APPDataBase *sharedDataBase = [APPDataBase sharedDataBase];
    self.navigationItem.hidesBackButton = YES;
    if ([self testInternetConnection]){
        recievedArray = [recievedURL componentsSeparatedByString:@" "];
        [self fillUpTableViewWithTitles];
        [sharedDataBase saveArrayData:feeds WithKey:@"savedFeeds"];
    }
    else
    {
        //проверить не получается, эмулятор онлайн даже при выключенном интернете
        feeds = [sharedDataBase loadDataWithKey:@"savedFeeds"];
    }
}

-(BOOL)testInternetConnection{
    internetReachableFoo = [Reachability reachabilityWithHostname:@"www.ya.ru"];
    
    internetReachableFoo.reachableBlock = ^(Reachability *reach){
        dispatch_async(dispatch_get_main_queue(), ^{
            //success
            isOnline = YES;
        });
    };
    
    internetReachableFoo.unreachableBlock = ^(Reachability *reach){
        dispatch_async(dispatch_get_main_queue(), ^{
            //not success
            isOnline = NO;
        });
    };
    return [internetReachableFoo startNotifier];
}

- (void)fillUpTableViewWithTitles {
    feeds = [[NSMutableArray alloc] init];
    NSURL *url;
    
    for (NSString *curUrl in recievedArray) {
        if (![curUrl  isEqual: @""]) {
            url = [NSURL URLWithString:[curUrl stringByReplacingOccurrencesOfString:@" " withString:@""]];
            parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
            [parser setDelegate:self];
            [parser setShouldResolveExternalEntities:NO];
            [parser parse];
            }
    }
    self.searchResult = [NSMutableArray arrayWithCapacity:[self.tableView numberOfRowsInSection:0]];
}

-(void)filterContentForSearchText:(NSString *)searchText{
    [self.searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    self.searchResult = [NSMutableArray arrayWithArray:[[feeds valueForKey:@"title"]  filteredArrayUsingPredicate:resultPredicate]];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView){
        return [self.searchResult count];
    }
    else
    {
        return feeds.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        cell.textLabel.text = [self.searchResult objectAtIndex:indexPath.row];
    }
    else
    {
    cell.textLabel.text = [[feeds objectAtIndex:indexPath.row] objectForKey: @"title"];
    }
    return cell;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        
        item    = [[NSMutableDictionary alloc] init];
        title   = [[NSMutableString alloc] init];
        link    = [[NSMutableString alloc] init];
        descr = [[NSMutableString alloc] init];
        pubDate = [[NSMutableString alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        
        [item setObject:title forKey:@"title"];
        [item setObject:link forKey:@"link"];
        [item setObject:descr forKey:@"description"];
        [item setObject:pubDate forKey:@"pubDate"];
        [feeds addObject:[item copy]];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"title"]) {
        [title appendString:string];
    } else if ([element isEqualToString:@"link"]) {
        [link appendString:string];
    } else if ([element isEqualToString:@"description"]){
        [descr appendString:string];
    } else if ([element isEqualToString:@"pubDate"]){
        [pubDate appendString:string];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    [self.tableView reloadData];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        NSArray *string = feeds[indexPath.row];
        APPDetailViewController *destViewController = segue.destinationViewController;
        destViewController.recievedNews = [string copy];
    }
}

- (IBAction)refreshButtonClick:(id)sender {
    [self fillUpTableViewWithTitles];
    [self showAlertWindow];
}

-(void)showAlertWindow{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Обновление успешно" message:@"Нажмите ОК." preferredStyle:UIAlertControllerStyleAlert];
    alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self okButtonTapped];
    }]];
    
    UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topRootViewController.presentedViewController){
        topRootViewController = topRootViewController.presentedViewController;
    }
    
    [topRootViewController presentViewController:alertController animated:YES completion:nil];
}

-(void) okButtonTapped{};

@end
