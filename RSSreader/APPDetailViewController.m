//
//  APPDetailViewController.m
//  RSSreader
//
//  Created by Rafael Garcia Leiva on 08/04/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "APPDetailViewController.h"
#import "RTLabel.h"


#define TICK NSDate *startTime = [NSDate date]
#define TOCK NSLog(@"Function: %@, Time: %f", NSStringFromSelector(_cmd), -[startTime timeIntervalSinceNow])

@implementation APPDetailViewController

@synthesize recievedNews;

#pragma mark - Managing the detail item

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getObjects];
}

- (void) getObjects{
    self.nameOfPage.title = [recievedNews valueForKey:@"title"];
    NSString *markup = [[recievedNews valueForKey:@"pubDate"] stringByAppendingString:[recievedNews valueForKey:@"description"]];
    NSScanner *scanner = [NSScanner scannerWithString:markup];
    NSString *imageString, *descriptionString;
    
    [scanner scanUpToString:@"src=\"" intoString:nil];
    [scanner scanString:@"src=\"" intoString:nil];
    [scanner scanUpToString:@"\"" intoString:&imageString];
    
    RTLabel *attr =[[RTLabel alloc] initWithFrame:CGRectZero];
    //[self.textPreviewView addSubview:attr];
    [attr setText:markup];
    
    NSString *tmpStr = [attr plainText];
    descriptionString = [tmpStr copy];
    
    [self.textPreviewView setScrollEnabled:NO];
    self.textPreviewView.text = descriptionString;
    
    //TO DO - НАВЕСТИ МАРАФЕТ!!!!!
    
    NSURL *urlImage = [NSURL URLWithString:imageString];
    NSData *data = [NSData dataWithContentsOfURL:urlImage];
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:descriptionString];
    NSTextAttachment *myAttachment = [[NSTextAttachment alloc] init];
    myAttachment.image = [UIImage imageWithData:data];
    
    CGFloat oldWidth = myAttachment.image.size.width;
    
    CGFloat scaleFactor = oldWidth / (self.textPreviewView.frame.size.width - 10);
    
    myAttachment.image = [UIImage imageWithCGImage:myAttachment.image.CGImage scale:scaleFactor orientation:UIImageOrientationUp];
    
    NSAttributedString *attrStringWithImage = [NSAttributedString attributedStringWithAttachment:myAttachment];
    [attString insertAttributedString:attrStringWithImage atIndex:0];
    self.textPreviewView.attributedText = attString;
    
    [self.textPreviewView setScrollEnabled:YES];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Открыть" style:UIBarButtonItemStylePlain target:self action:@selector(buttonClicked)];
    
    self.navigationItem.rightBarButtonItem = anotherButton;
}

- (void)buttonClicked {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[recievedNews valueForKey:@"link"]]];
}


@end
