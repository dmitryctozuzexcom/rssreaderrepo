//
//  APPDetailViewController.h
//  RSSreader
//
//  Created by Rafael Garcia Leiva on 08/04/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APPDetailViewController : UIViewController

@property (copy, nonatomic) NSString *url;

@property (copy, nonatomic) NSMutableArray *recievedNews;

@property (strong, nonatomic) IBOutlet UITextView *textPreviewView;

@property (weak, nonatomic) IBOutlet UINavigationItem *nameOfPage;

@end
