//
//  APPPreMasterViewController.m
//  RSSreader
//
//  Created by Mac on 02.02.16.
//  Copyright © 2016 Appcoda. All rights reserved.
//

#import "APPPreMasterViewController.h"
#import "APPMasterViewController.h"
#import "APPDataBase.h"

@interface APPPreMasterViewController () {
    NSArray *items;
    NSMutableString *title;
    NSMutableString *link;
    NSString *element;
    NSArray *webSitesNames;
    NSMutableString *urlsToSend;
    NSString *isFirstTimeLaunched;
}
@end

@implementation APPPreMasterViewController

- (void)viewDidLoad {
    APPDataBase *sharedDataBase = [APPDataBase sharedDataBase];
    [sharedDataBase setUpDataBase];
    
    isFirstTimeLaunched = [sharedDataBase loadDataWithKey:@"savedIsFirstLaunch"];
    
    if (!isFirstTimeLaunched)
    {
        [self.sendButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        isFirstTimeLaunched = NO;
        //check
    }
    else
    {
        [super viewDidLoad];
        [self showAlertWindow];
        [self initializeSitesArrays];
    }
}


-(void)showAlertWindow{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Добро пожаловать!" message:@"Благодарим за установку приложения, для начала выберите интересующие Вас категории." preferredStyle:UIAlertControllerStyleAlert];
    alertController.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self okButtonTapped];
    }]];

    UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topRootViewController.presentedViewController){
        topRootViewController = topRootViewController.presentedViewController;
    }
    
    [topRootViewController presentViewController:alertController animated:YES completion:nil];
}

-(void)initializeSitesArrays{
    items = [NSArray arrayWithObjects:
             @"http://feeds.dzone.com/home",
             @"http://feeds.dzone.com/publications",
             @"http://feeds.dzone.com/agile",
             @"http://feeds.dzone.com/big-data",
             @"http://feeds.dzone.com/cloud",
             @"http://feeds.dzone.com/database",
             @"http://feeds.dzone.com/devops",
             @"http://feeds.dzone.com/integration",
             @"http://feeds.dzone.com/iot",
             @"http://feeds.dzone.com/java",
             @"http://feeds.dzone.com/mobile",
             @"http://feeds.dzone.com/performance",
             @"http://feeds.dzone.com/webdev", nil];
    webSitesNames = [NSArray arrayWithObjects:
                     @"Home",
                     @"Refcardz and Guides",
                     @"Agile Zone",
                     @"Big Data Zone",
                     @"Cloud Zone",
                     @"Database Zone",
                     @"DevOps Zone",
                     @"Integration Zone",
                     @"IoT Zone",
                     @"Java Zone",
                     @"Mobile Zone",
                     @"Performance Zone",
                     @"Web Dev Zone", nil];
    self.cellSelected = [NSMutableArray array];
}

-(void)viewDidAppear:(BOOL)animated{
    [self.sourceTableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showNews"])
    {
        APPDataBase *sharedDataBase = [APPDataBase sharedDataBase];
        APPMasterViewController *destViewController = segue.destinationViewController;
        if (isFirstTimeLaunched)
        {
            isFirstTimeLaunched = NO;
            [sharedDataBase saveData:@"NO" WithKey:@"savedIsFirstLaunch"];
            urlsToSend = [NSMutableString string];
            for (NSIndexPath *curCellPath in self.cellSelected) {
                [urlsToSend appendString:[items objectAtIndex:curCellPath.row]];
                [urlsToSend appendString:@" "];
            }
            [sharedDataBase saveData:urlsToSend WithKey:@"savedUrlToSend"];
        }
        else
        {
            [sharedDataBase loadDataWithKey:@"savedUrlToSend"];
        }
        destViewController.recievedURL = [urlsToSend copy];
    }
}

#pragma mark - Source Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)sourceTableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)sourceTableView numberOfRowsInSection:(NSInteger)section {
    return items.count;
}

- (UITableViewCell *)tableView:(UITableView *)sourceTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *myIdentifier = @"Cell";
    UITableViewCell *cell = [sourceTableView dequeueReusableCellWithIdentifier:myIdentifier forIndexPath:indexPath];
    
    NSString *curName = [webSitesNames objectAtIndex:indexPath.row];
    cell.textLabel.text = [curName copy];
    if([self.cellSelected containsObject:indexPath]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
        
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.cellSelected containsObject:indexPath]) {
        [self.cellSelected removeObject:indexPath];
    } else {
        [self.cellSelected addObject:indexPath];
    }
    [tableView reloadData];
}

-(void) okButtonTapped{};

@end
